package com.cutthecord.commands;

public interface EnableDisableHandler {

    String processEnableDisable(boolean enabled);

}
