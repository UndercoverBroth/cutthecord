package com.cutthecord.commands;

public interface RawMsgHandler {

    String processRawMessage(String orig);

}
